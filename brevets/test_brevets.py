import arrow
from acp_times import open_time, close_time

testtime = arrow.get('2020/01/01 00:00')

def test_zero_km_openclose():
    """
    Tests expected open time for zero km
    Ensures that close time is calculated differently for 0 km
    """
    assert open_time(0, 200, testtime.isoformat()) == testtime.isoformat()
    assert close_time(0, 200, testtime.isoformat()) == arrow.get('2020/01/01 01:00').isoformat()

def test_200_km_openclose():
    """
    Tests expected open & close times for 200 km
    Ensures that the 200km brevet special rule is adhered to
    """
    assert open_time(200, 200, testtime.isoformat()) == arrow.get('2020/01/01 05:53').isoformat()
    assert close_time(200, 200, testtime.isoformat()) == arrow.get('2020/01/01 13:30').isoformat()
    assert close_time(200, 1000, testtime.isoformat()) == arrow.get('2020/01/01 13:20').isoformat()

def test_550_km_openclose():
    """
    Tests expected open & close times for 550 km
    Catches minute-rounding errors
    incorrect rounding results in an open time of 17:07
    """
    assert open_time(550, 600, testtime.isoformat()) == arrow.get('2020/01/01 17:08').isoformat()
    assert close_time(550, 600, testtime.isoformat()) == arrow.get('2020/01/02 12:40').isoformat()

def test_890_km_openclose():
    """
    Tests expected open & close times for 890 km control
    Ensures that algorithm for close time works across boundaries
    """
    assert open_time(890, 1000, testtime.isoformat()) == arrow.get('2020/01/02 05:09').isoformat()
    assert close_time(890, 1000, testtime.isoformat()) == arrow.get('2020/01/03 17:23').isoformat()

def test_1000_km_openclose():
    """
    Tests expected open & close times for 1000 km
    Ensures that maximum distance works appropriately
    """
    assert open_time(1000, 1000, testtime.isoformat()) == arrow.get('2020/01/02 09:05').isoformat()
    assert close_time(1000, 1000, testtime.isoformat()) == arrow.get('2020/01/04 03:00').isoformat()
# ACP Time Calculator
Brevets are a type of long-distance bicycle ride with several control points along the way. Each of these
control points has a time when it opens and closes, based on the distance of the control from the start
of the brevet and the minimum or maximum speed of the riders in the brevet.

This codebase allows you to calculate the open and close times for different control points along a brevet.
The code uses the RUSA guidelines for calculating the open and close times, as can be found here:
https://rusa.org/pages/acp-brevet-control-times-calculator

The maximum brevet distance is 1000 km, at least for all countries outside France, and so this codebase only
supports controls placed up to 1000 km from the start of the brevet.

## For Users
To use this codebase, build and run the Dockerfile included in the brevets directory. Once running, visit the
website and enter the desired start time, start date, control distances, and total brevet distance. After entering
each control's distance from the start of the brevet, the page will automatically update with the open and close
times for that control.

## For Developers
There is a suite of nosetests included that ensure the proper functioning of the code. Key distances are used
to distinguish between correct and incorrect implementations of the RUSA guidelines. The nosetests must be called
inside a running container, from the brevets directory, using the command 'nosetests3'.

The current implementation does not provide error messages to the user for invalid inputs, it would be keen to
include these features in a future version.

The current implementation uses Flask, AJAX, and python to manage requests to/from the webpage.

## Author Information
* Author: Thomas Kismarton
* Contact email: tkismar2@uoregon.edu
* Function: Hosts a website that computes brevet control times